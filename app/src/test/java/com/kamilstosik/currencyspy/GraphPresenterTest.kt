package com.kamilstosik.currencyspy

import com.kamilstosik.currencyspy.dagger.CurrencySpyAppComponent
import com.kamilstosik.currencyspy.dagger.Injector
import com.kamilstosik.currencyspy.graph.GraphContract
import com.kamilstosik.currencyspy.graph.GraphPresenter
import com.kamilstosik.currencyspy.repository.CurrencyRepository
import org.junit.Test

import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.runners.MockitoJUnitRunner
import java.text.SimpleDateFormat
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class GraphPresenterTest  {

    val presenter = GraphPresenter()
    val viewMock = mock(GraphContract.View::class.java)
    val currencyRepositoryMock = mock(CurrencyRepository::class.java)

    @Before
    fun setup() {
        val component = mock(CurrencySpyAppComponent::class.java)
        val injector = mock(Injector::class.java)
        `when`(injector.getComponentFromInstance()).thenReturn(component)
        presenter.attach(viewMock)
        presenter.currencyRepository = currencyRepositoryMock

        val datesList = arrayListOf<String>()
        val datesListMock = spy(datesList)
        currencyRepositoryMock.datesList = datesListMock
        doNothing().`when`(currencyRepositoryMock.datesList).clear()

        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        currencyRepositoryMock.formatter = formatter
    }

    @Test
    fun loadChartDataTest() {
        val TEST_START_DATE = "2018-02-20"
        val TEST_END_DATE = "2018-03-20"
        val TEST_SYMBOLS = "USD"
        val TEST_KEY = "123456"

        `when`(viewMock.getStartDate()).thenReturn(TEST_START_DATE)
        `when`(viewMock.getEndDate()).thenReturn(TEST_END_DATE)
        `when`(viewMock.getFixerApiKey()).thenReturn(TEST_KEY)

        presenter.generateChartData()
        verify(currencyRepositoryMock, times(1)).subscribeAndFetchCurrencyExchangeRates(TEST_START_DATE, TEST_END_DATE, TEST_SYMBOLS, TEST_KEY, presenter)
    }

    @Test
    fun loadChartDataWrongDatesTest() {
        val TEST_START_DATE = "2018-04-20"
        val TEST_END_DATE = "2018-03-20"
        val TEST_SYMBOLS = "USD"
        val TEST_KEY = "123456"

        `when`(viewMock.getStartDate()).thenReturn(TEST_START_DATE)
        `when`(viewMock.getEndDate()).thenReturn(TEST_END_DATE)
        `when`(viewMock.getFixerApiKey()).thenReturn(TEST_KEY)
        presenter.generateChartData()
        verify(currencyRepositoryMock, times(1)).subscribeAndFetchCurrencyExchangeRates(TEST_START_DATE, TEST_END_DATE, TEST_SYMBOLS, TEST_KEY, presenter)
    }

    @Test
    fun handleError() {
        val error = Throwable("test error")
        presenter.handleError(error)
        verify(viewMock, times(1)).showError("test error")
    }

    @Test
    fun handleResults() {
        val sortedMapTest = sortedMapOf<Long, Double>()
        presenter.handleResults(sortedMapTest)
        verify(viewMock, times(1)).updateGraph(sortedMapTest)
    }
}
