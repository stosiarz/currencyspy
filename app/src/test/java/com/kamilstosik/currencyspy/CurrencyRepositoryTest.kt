package com.kamilstosik.currencyspy

import com.kamilstosik.currencyspy.api.ApiImplementation
import com.kamilstosik.currencyspy.dagger.CurrencySpyAppComponent
import com.kamilstosik.currencyspy.dagger.Injector
import com.kamilstosik.currencyspy.graph.GraphModel
import com.kamilstosik.currencyspy.repository.CurrencyRepository
import com.kamilstosik.currencyspy.repository.ExchangeRateListener
import junit.framework.Assert.*
import org.junit.Test

import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.runners.MockitoJUnitRunner
import java.text.SimpleDateFormat
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class CurrencyRepositoryTest  {

    val repository = spy(CurrencyRepository())
    val apiMock = mock(ApiImplementation::class.java)
    val listener = mock(ExchangeRateListener::class.java)

    @Before
    fun setup() {
        val component = mock(CurrencySpyAppComponent::class.java)
        val injector = mock(Injector::class.java)
        `when`(injector.getComponentFromInstance()).thenReturn(component)
        repository.api = apiMock
        repository.exchangeRateListener = listener
    }

    @Test
    fun subscribeAndFetchCurrencyExchangeRatesTest() {
        val listener = mock(ExchangeRateListener::class.java)
        val TEST_START_DATE = "2018-02-20"
        val TEST_END_DATE = "2018-03-20"
        val TEST_SYMBOLS = "USD"
        val TEST_KEY = "123456"
        repository.subscribeAndFetchCurrencyExchangeRates(TEST_START_DATE, TEST_END_DATE, TEST_SYMBOLS, TEST_KEY, listener)
        verify(apiMock, times(1)).fetchCurrencyExchangeRates(TEST_START_DATE, TEST_SYMBOLS, TEST_KEY, repository)
    }

    @Test
    fun subscribeAndFetchCurrencyExchangeRatesWrongDateTest() {
        val listener = mock(ExchangeRateListener::class.java)
        val TEST_START_DATE = "2018-04-20"
        val TEST_END_DATE = "2018-03-20"
        val TEST_SYMBOLS = "USD"
        val TEST_KEY = "123456"
        repository.subscribeAndFetchCurrencyExchangeRates(TEST_START_DATE, TEST_END_DATE, TEST_SYMBOLS, TEST_KEY, listener)
        verify(apiMock, times(1)).fetchCurrencyExchangeRates(TEST_START_DATE, TEST_SYMBOLS, TEST_KEY, repository)
    }

    @Test
    fun handleResultsTest() {
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val TEST_START_DATE = "2018-02-20"
        val exchangeRate = 1.34567
        val rates = mutableMapOf<String, Double>()
        val datesListStub = arrayListOf<String>()
        val output = sortedMapOf<Long, Double>()

        val timestamp = formatter.parse(TEST_START_DATE).time

        datesListStub.add(TEST_START_DATE)
        repository.datesList = datesListStub
        rates["USD"] = exchangeRate
        output[timestamp] = exchangeRate

        val data = GraphModel(true, "123456", true, "USD", TEST_START_DATE, rates)
        repository.handleResults(data)

        assertEquals(1,repository.apiCounter)
        assertEquals(1,repository.datesList.size)
        assertEquals(repository.apiCounter,repository.datesList.size)

        verify(repository.exchangeRateListener, times(1)).handleResults(output)
    }

    @Test
    fun handleErrorTest() {
        val throwMock = mock(Throwable::class.java)
        repository.handleError(throwMock)
    }
}
