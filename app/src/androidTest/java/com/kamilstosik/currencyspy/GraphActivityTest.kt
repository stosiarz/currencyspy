package com.kamilstosik.currencyspy

import androidx.test.espresso.Espresso.onView
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Test

import org.junit.runner.RunWith
import org.junit.Rule
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.kamilstosik.currencyspy.graph.GraphActivity



@RunWith(AndroidJUnit4::class)
class MapActivityTest  {

    @Rule @JvmField
    val activityTestRule: ActivityTestRule<GraphActivity> = ActivityTestRule(GraphActivity::class.java)

    @Test
    fun verifyGraph() {
        onView(withId(R.id.chart)).check(matches(isDisplayed()))
    }
}
