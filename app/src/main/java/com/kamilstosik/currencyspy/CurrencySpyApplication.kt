package com.kamilstosik.currencyspy

import android.app.Application
import com.kamilstosik.currencyspy.dagger.AppModule
import com.kamilstosik.currencyspy.dagger.CurrencySpyAppComponent
import com.kamilstosik.currencyspy.dagger.CurrencySpyModule
import com.kamilstosik.currencyspy.dagger.DaggerCurrencySpyAppComponent

class CurrencySpyApplication : Application() {

    companion object {
        private var INSTANCE: CurrencySpyApplication? = null
        fun getInstance(): CurrencySpyApplication? = INSTANCE
    }

    lateinit var component: CurrencySpyAppComponent

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        component = DaggerCurrencySpyAppComponent.builder().appModule(AppModule(this)).currencySpyModule(CurrencySpyModule()).build()
    }

}