package com.kamilstosik.currencyspy.graph

import com.google.gson.annotations.SerializedName

data class GraphModel(@SerializedName("success")
                      val success: Boolean,
                      @SerializedName("timestamp")
                      val timestamp: String,
                      @SerializedName("historical")
                      val historical: Boolean,
                      @SerializedName("base")
                      val base: String,
                      @SerializedName("date")
                      val date: String,
                      @SerializedName("rates")
                      val rates: Map<String, Double>)


