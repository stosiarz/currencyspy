package com.kamilstosik.currencyspy.graph

import java.util.*

class GraphContract {
    interface View {
        fun getFixerApiKey() : String
        fun updateGraph(exchangeRateMap: SortedMap<Long, Double>)
        fun attachXAxisCustomConverter()
        fun showError(error: String)
        fun getStartDate() : String
        fun getEndDate() : String
    }

    interface Presenter {
        fun attach(view: GraphContract.View)
        fun setupChart()
        fun generateChartData()

    }
}