package com.kamilstosik.currencyspy.graph

import com.kamilstosik.currencyspy.dagger.Injector
import com.kamilstosik.currencyspy.repository.CurrencyRepository
import com.kamilstosik.currencyspy.repository.CurrencyRepository.Companion.SYMBOL_USD
import com.kamilstosik.currencyspy.repository.ExchangeRateListener
import java.util.*
import javax.inject.Inject

open class GraphPresenter : GraphContract.Presenter, ExchangeRateListener {

    @Inject
    lateinit var currencyRepository: CurrencyRepository

    private lateinit var view: GraphContract.View

    init {
        Injector().mainComponent?.inject(this)
    }

    override fun attach(view: GraphContract.View) {
        this.view = view
    }

    override fun setupChart() {
        view.attachXAxisCustomConverter()
    }

    override fun generateChartData() {
        currencyRepository.subscribeAndFetchCurrencyExchangeRates(view.getStartDate(),
            view.getEndDate(),
            SYMBOL_USD,
            view.getFixerApiKey(),
            this)
    }

    /**
     * Notify view when data is ready to draw the chart
     */
    override fun handleResults(exchangeRateMap: SortedMap<Long, Double>) {
        view.updateGraph(exchangeRateMap)
    }

    override fun handleError(t: Throwable) {
        t.message?.let { view.showError(it) }
    }
}