package com.kamilstosik.currencyspy.graph

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.Entry
import com.kamilstosik.currencyspy.R
import com.kamilstosik.currencyspy.dagger.Injector
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.formatter.IAxisValueFormatter

/**
 * Main activity displays chart of exchange rate Euro to Dollar within given range of dates
 */
class GraphActivity : AppCompatActivity(), GraphContract.View {

    @Inject
    lateinit var presenter : GraphPresenter

    private val entries = arrayListOf<Entry>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Injector().mainComponent?.inject(this)
        presenter.attach(this)
        presenter.setupChart()
        presenter.generateChartData()
    }

    override fun attachXAxisCustomConverter() {
        chart.xAxis.valueFormatter = MyCustomXAxisValueFormatter()
    }

    override fun updateGraph(exchangeRateMap: SortedMap<Long, Double>) {
        populateEntries(exchangeRateMap)
        val dataSet = LineDataSet(entries, getString(R.string.label))
        chart.data = LineData(dataSet)
        chart.invalidate()
    }

    override fun getFixerApiKey(): String {
        return getString(com.kamilstosik.currencyspy.R.string.fixer_key)
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    /**
     * TODO Should come from UI
     */
    override fun getStartDate() : String {
        return "2018-02-20"
    }

    /**
     * TODO Should come from UI
     */
    override fun getEndDate() : String {
        return "2018-03-20"
    }

    private fun populateEntries(exchangeRateMap: SortedMap<Long, Double>) {
        exchangeRateMap.forEach {
            entries.add(Entry(it.key.toFloat(), it.value.toFloat()))
        }
    }

    private class MyCustomXAxisValueFormatter : IAxisValueFormatter {
        private val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        override fun getFormattedValue(value: Float, axis: AxisBase?): String {
            return formatter.format(value)
        }
    }
}
