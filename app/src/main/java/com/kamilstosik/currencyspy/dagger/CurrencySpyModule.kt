package com.kamilstosik.currencyspy.dagger

import com.kamilstosik.currencyspy.api.ApiImplementation
import com.kamilstosik.currencyspy.api.ApiManager
import com.kamilstosik.currencyspy.graph.GraphPresenter
import com.kamilstosik.currencyspy.repository.CurrencyRepository
import dagger.Module
import dagger.Provides

@Module
class CurrencySpyModule {

    @Provides
    fun provideListPresenter(): GraphPresenter {
        return GraphPresenter()
    }

    @Provides
    fun provideCurrencyRepository(): CurrencyRepository {
        return CurrencyRepository()
    }

    @Provides
    fun provideApiImplementation(): ApiImplementation {
        return ApiImplementation()
    }

    @Provides
    fun provideApiManager(): ApiManager {
        return ApiManager.instance
    }
}