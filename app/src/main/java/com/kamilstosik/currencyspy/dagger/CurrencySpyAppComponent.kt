package com.kamilstosik.currencyspy.dagger

import com.kamilstosik.currencyspy.CurrencySpyApplication
import com.kamilstosik.currencyspy.api.ApiImplementation
import com.kamilstosik.currencyspy.graph.GraphActivity
import com.kamilstosik.currencyspy.graph.GraphPresenter
import com.kamilstosik.currencyspy.repository.CurrencyRepository
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, CurrencySpyModule::class])
@Singleton
interface CurrencySpyAppComponent {
    fun inject(app: CurrencySpyApplication)
    fun inject(graphActivity: GraphActivity)
    fun inject(graphPresenter: GraphPresenter)
    fun inject(currencyRepository: CurrencyRepository)
    fun inject(apiImplementation: ApiImplementation)

}