package com.kamilstosik.currencyspy.dagger

import com.kamilstosik.currencyspy.CurrencySpyApplication

open class Injector {
    var mainComponent : CurrencySpyAppComponent? = getComponentFromInstance()

    open fun getComponentFromInstance() : CurrencySpyAppComponent? {
        return CurrencySpyApplication.getInstance()?.component
    }
}