package com.kamilstosik.currencyspy.dagger

import android.content.Context
import com.kamilstosik.currencyspy.CurrencySpyApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: CurrencySpyApplication) {
    @Provides
    @Singleton
    fun provideApp() = app

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext
}