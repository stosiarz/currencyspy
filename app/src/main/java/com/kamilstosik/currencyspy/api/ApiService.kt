package com.kamilstosik.currencyspy.api

import com.kamilstosik.currencyspy.graph.GraphModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    /**
     * Example url to connect to get currency exchange date
     * http://data.fixer.io/api/latest?access_key=<ACCESS_KEY>
     */
    @GET("{date}")
    fun currencyExchangeList(
        @Path("date") date: String,
        @Query("access_key") access_key: String,
        @Query("symbols") symbols: String) : Observable<GraphModel>
}