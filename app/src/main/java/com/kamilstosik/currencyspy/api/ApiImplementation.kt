package com.kamilstosik.currencyspy.api

import com.kamilstosik.currencyspy.dagger.Injector
import com.kamilstosik.currencyspy.graph.GraphModel
import com.kamilstosik.currencyspy.repository.CurrencyRepositoryListener
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

open class ApiImplementation : ApiCalls {

    @Inject
    lateinit var apiManager: ApiManager

    init {
        Injector().mainComponent?.inject(this)
    }

    override fun fetchCurrencyExchangeRates(date: String, symbols: String, key : String, listener: CurrencyRepositoryListener) {
        getCurrencyExchangeRateObservable(date, symbols, key).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (listener::handleResults, listener::handleError)
    }

    private fun getCurrencyExchangeRateObservable(date: String, symbols: String, key : String) : Observable<GraphModel> {
        return apiManager.service.currencyExchangeList(date, key, symbols)
    }
}