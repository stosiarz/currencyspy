package com.kamilstosik.currencyspy.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiManager {

    companion object {
        private const val URL_CURRENCY = "http://data.fixer.io/api/"
        val instance = ApiManager()
    }

    private val logging = HttpLoggingInterceptor()
    private val retrofit: Retrofit

    val service : ApiService

    init {
        retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(buildClient())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(URL_CURRENCY)
            .build()

        service = retrofit.create(ApiService::class.java)
    }

    private fun buildClient() : OkHttpClient {
        logging.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient().newBuilder()
            .addInterceptor(logging)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()
    }
}