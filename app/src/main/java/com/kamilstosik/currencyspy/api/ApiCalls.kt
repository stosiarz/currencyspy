package com.kamilstosik.currencyspy.api

import com.kamilstosik.currencyspy.repository.CurrencyRepositoryListener

/**
 * List all the api calls available to the application
 */
interface ApiCalls {
    fun fetchCurrencyExchangeRates(date: String, symbols: String, key : String, listener: CurrencyRepositoryListener)
}