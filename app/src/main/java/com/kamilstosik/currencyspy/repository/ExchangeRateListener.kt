package com.kamilstosik.currencyspy.repository

import java.util.*

open interface ExchangeRateListener {
    fun handleResults(exchangeRateMap: SortedMap<Long, Double>)
    fun handleError(t : Throwable)

}