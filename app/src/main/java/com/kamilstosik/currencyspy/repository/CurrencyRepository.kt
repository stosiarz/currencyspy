package com.kamilstosik.currencyspy.repository

import com.kamilstosik.currencyspy.api.ApiImplementation
import com.kamilstosik.currencyspy.dagger.Injector
import com.kamilstosik.currencyspy.graph.GraphModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

/**
 * Currency repository calls Api to fetch exchange rate Euro to Dollar within given date range
 */
open class CurrencyRepository : CurrencyRepositoryListener{

    companion object {
        private const val DAY_IN_MILISECONDS = 86400000L
        private const val API_DATE_FORMAT = "yyyy-MM-dd"
        const val SYMBOL_USD = "USD"
    }

    @Inject
    lateinit var api: ApiImplementation

    lateinit var exchangeRateListener : ExchangeRateListener

    private val exchangeDataMap = hashMapOf<Long, Double>()

    var formatter = SimpleDateFormat(API_DATE_FORMAT, Locale.US)
    var datesList = arrayListOf<String>()
    var apiCounter = 0

    init {
        Injector().mainComponent?.inject(this)
    }

    /**
     * Api is able to handle only one date at a time therefore
     * calculate the range of date and call api for each of them
     * Notify presenter when all exchanges are fetched
     */
    open fun subscribeAndFetchCurrencyExchangeRates(startDate: String, endDate: String, symbols: String, key: String, listener: ExchangeRateListener) {
        resetCounters()
        exchangeRateListener = listener
        calculateDates(startDate, endDate).map { dateString ->
            datesList.add(dateString)
            api.fetchCurrencyExchangeRates(dateString, symbols, key, this@CurrencyRepository)
        }
    }

    override fun handleResults(currency: GraphModel) {
        incrementCounterOnApiResult()
        formatExchangeRateData(currency)
        notifyOnLastDate()
    }

    override fun handleError(t: Throwable) {
        incrementCounterOnApiResult()
        showErrorOnLastApiCall(t)
    }

    private fun formatExchangeRateData(currency: GraphModel) {
        val date = formatter.parse(currency.date)
        currency.rates[SYMBOL_USD]?.let {
            exchangeDataMap[date.time] = it
        }
    }

    private fun incrementCounterOnApiResult() {
        apiCounter++
    }

    private fun notifyOnLastDate() {
        if(isLastExchangeRateApiCallPerformed()) {
            exchangeRateListener.handleResults(exchangeDataMap.toSortedMap())
        }
    }

    /**
     * Calculated range of dates from start to end as list of Strings
     * Format required by backend
     */
    private fun calculateDates(startDate: String, endDate: String) : ArrayList<String> {
        val start = parseDateStringToMiliseconds(startDate)
        val end = parseDateStringToMiliseconds(endDate)
        return getRangeOfDatesAsList(start, end)
    }

    private fun getRangeOfDatesAsList(start: Long, end: Long) : ArrayList<String>{
        val dates = arrayListOf<String>()
        var counter = start

        dates.add(timestampToFormattedDate(start))
        while (counter < end) {
            counter += DAY_IN_MILISECONDS
            dates.add(timestampToFormattedDate(counter))
        }
        return dates
    }

    private fun timestampToFormattedDate(nextDateMilliseconds : Long) = formatter.format(Date(nextDateMilliseconds))

    private fun showErrorOnLastApiCall(t: Throwable) {
        if(isLastExchangeRateApiCallPerformed()) {
            exchangeRateListener.handleError(t)
        }
    }

    private fun parseDateStringToMiliseconds(date: String) : Long {
        return formatter.parse(date).time
    }

    private fun isLastExchangeRateApiCallPerformed() : Boolean {
        return apiCounter == datesList.size
    }

    private fun resetCounters() {
        apiCounter = 0
        datesList.clear()
    }
}