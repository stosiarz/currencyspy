package com.kamilstosik.currencyspy.repository

import com.kamilstosik.currencyspy.graph.GraphModel

interface CurrencyRepositoryListener {
    fun handleError(t: Throwable)
    fun handleResults(currency: GraphModel)
}